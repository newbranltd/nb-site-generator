#!/usr/bin/env node
const debug = require('debug')('nb-site-generator:cli')
const fsx = require('fs-extra')
const { join } = require('path')
const baseDir = process.cwd()
const {
  DEFAULT_SITE_PORT,
  DEFAULT_EDITOR_PORT,
  DEFAULT_CONFIG_FILE,
  DEFAULT_THEME_DIR,
  DEFAULT_PROJECT_DIR,
  DEFAULT_DIST_DIR
} = require('./src/constants')
const { serve, build } = require('./src')

// run
require('yargs') // eslint-disable-line
  .command('serve [project] [port]', `start the doc site server and editor, default port ${DEFAULT_SITE_PORT}`, (yargs) => {
    yargs
      .positional('project', {
        describe: 'Where the project files will be',
        default: DEFAULT_PROJECT_DIR
      })
      .positional('port', {
        describe: 'port for doc site to bind on',
        default: DEFAULT_SITE_PORT
      })
  }, (argv) => {
    if (argv.verbose) {
      console.info(`Start docsite on :${argv.port} and editor on ${argv.editorPort}`)
    }
    serve(argv)
  })
  .command('build [dist]', `build the site using the information we gathered`, yargs => {
    // nothing to do here
    yargs.
      positional('dist', {
        describe: 'Where your built files will be',
        default: DEFAULT_DIST_DIR
      })
  }, argv => {
    if (argv.verbose) {
      console.info(`Start building ...`)
    }
    build(argv)
  })
  .option('verbose', {
    alias: 'v',
    default: false
  })
  .option('editorPort', {
    alias: 'e',
    default: DEFAULT_EDITOR_PORT,
    describe: 'editor port to bind on, if not provide then it will be port + 1'
  })
  .option('theme', {
    alias: 't',
    default: DEFAULT_THEME_DIR,
    describe: 'Theme folder to use'
  })
  .options('dist', {
    alias: 'd',
    default: DEFAULT_DIST_DIR,
    describe: 'Where the distribute version will be'
  })
  .option('configFile', {
    alias: 'c',
    describe: 'Config file for setting up the project',
    default: DEFAULT_CONFIG_FILE
  })
  .argv
