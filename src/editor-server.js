const serverIoCore = require('server-io-core')
const koaMiddleware = require('jsonql-koa')
const { join, resolve } = require('path')

/**
 * dont' really want to allow them to add too many options
 * @param {int} port port number to run
 */
module.exports = function(port) {
  serverIoCore({
    port,
    webroot: [
      join(__dirname, 'public', 'docsite'),
      resolve(join(__dirname, '..', 'node_modules'))
    ],
    debugger: false,
    reload: true,
    middlewares: [
      koaMiddleware({
        resolverDir: join(__dirname, 'app', 'resolvers'),
        contractDir: join(__dirname, 'app', 'contract')
      })
    ]
  })
}
