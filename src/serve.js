// take the serve function out from the cli.js
const editorServer = require('./editor-server')
const siteServer = require('./site-server')
const saveConfig = require('./save-config')
const debug = require('debug')('nb-site-generator:serve')
/**
 * sort out all the configuration option then start up the two server
 * @param {object} args from yargs
 */
module.exports = function serve(args) {
  debug('raw args', args)
  const configFile = join(baseDir, args.configFile)
  let createConfigFile = true;
  if (fsx.existsSync(configFile)) {
    args = require(configFile)
    createConfigFile = false;
  }
  debug('args', args)
  const themeDir = join(baseDir, args.theme)
  const projectDir = join(baseDir, args.project)
  if (!fsx.existsSync(projectDir)) {
    fsx.ensureDir(projectDir) // if it doesn't exist then create it
  }
  // start serving
  editorServer(args.editorPort)
  let config = {
    port: args.port,
    projectDir
  }
  if (fsx.existsSync(themeDir)) {
    config.theme = themeDir;
  }
  siteServer(config)
  // now we generate a config file
  if (createConfigFile) {
    saveConfig(config)
  }

}
