// just dev with bulma-start
const serverIoCore = require('server-io-core')
const { join } = require('path')

serverIoCore({
  webroot: [join(__dirname, 'bulma-start')],
  port: 4001
})
