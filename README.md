# nb-site-generator

A static site generator with an editor for the rest of us.

## idea

Just a simple 3 columns input, title, sidebar and main editor area (just write raw mark down) for the
time being.

When launch it will run two windows at the same time, one is the final output, and one is the editor.
Whenever the editor save, then the main window will update. And that's it.

  
